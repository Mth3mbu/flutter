import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class UserImagePicker extends StatefulWidget {
  final void Function(PickedFile file) onPickImage;
  UserImagePicker(this.onPickImage);

  @override
  _UserImagePickerState createState() => _UserImagePickerState();
}

class _UserImagePickerState extends State<UserImagePicker> {
  PickedFile _profilePic;
  void _pickImage() async {
    final ImagePicker _picker = ImagePicker();
    final image = await _picker.getImage(
        source: ImageSource.gallery, imageQuality: 50, maxWidth: 150);
    setState(() {
      _profilePic = image;
    });
    widget.onPickImage(_profilePic);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          radius: 50,
          backgroundColor: Colors.grey,
          backgroundImage:
              _profilePic != null ? FileImage(File(_profilePic.path)) : null,
        ),
        TextButton.icon(
          onPressed: _pickImage,
          icon: Icon(Icons.image),
          label: Text('Add Iamge'),
        )
      ],
    );
  }
}
