import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../chat/message_buble.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Messages extends StatelessWidget {
  const Messages({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var user = FirebaseAuth.instance.currentUser;
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chat')
          .orderBy('dateCreated', descending: true)
          .snapshots(),
      builder: (ctx, chatSnapShot) {
        if (chatSnapShot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        }

        final chatDocs = chatSnapShot.data?.docs ?? [];
        return ListView.builder(
            reverse: true,
            itemCount: chatDocs?.length,
            itemBuilder: (ctx, index) => MessageBuble(
                  chatDocs[index]['text'],
                  chatDocs[index]['userId'] == user.uid,
                  chatDocs[index]['username'],
                  chatDocs[index]['profile_pic'],
                  key: ValueKey(chatDocs[index].id),
                ));
      },
    );
  }
}
