import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../widgets/auth/auth_form.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';

class AuthScreen extends StatefulWidget {
  AuthScreen({Key key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var _isLoading = false;
  void _setIsloading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  void _authinticateUser(String email, String username, String password,
      PickedFile profilePic, bool isLogin, BuildContext ctx) async {
    var authResults;
    _setIsloading(true);
    try {
      await Firebase.initializeApp();
      final _auth = FirebaseAuth.instance;
      if (isLogin) {
        authResults = await _auth.signInWithEmailAndPassword(
            email: email, password: password);
      } else {
        authResults = await _auth.createUserWithEmailAndPassword(
            email: email, password: password);

        final store = FirebaseStorage.instance
            .ref()
            .child('profile_pictures')
            .child('${authResults.user.uid}.jpg');

        store.putFile(File(profilePic.path)).whenComplete(() async {
          var profilePicUrl = await store.getDownloadURL();
          await FirebaseFirestore.instance
              .collection('users')
              .doc(await authResults.user.uid)
              .set({
            'username': username,
            'email': email,
            'profile_pic': profilePicUrl
          });
        });
      }
    } on PlatformException catch (error) {
      var message = 'An error occured please check your credentils';
      if (error.message != null) {
        message = error.message;
      }
      Scaffold.of(ctx).showSnackBar(SnackBar(
        content: Text(message),
        backgroundColor: Colors.black,
      ));
      _setIsloading(false);
    } catch (error) {
      print(error);
      Scaffold.of(ctx).showSnackBar(SnackBar(
        content: Text(error.toString()),
        backgroundColor: Colors.black,
      ));
      _setIsloading(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: AuthForm(_authinticateUser, _isLoading));
  }
}
