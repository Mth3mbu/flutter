import 'package:flutter/material.dart';
import '../providers/product.dart';
import '../providers/products.dart';
import 'package:provider/provider.dart';

class ProductScreen extends StatefulWidget {
  static const routeName = '/add-product';

  const ProductScreen();

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _product =
      Product(id: null, title: '', description: '', price: 0, imageUrl: '');

  var _initValues = {
    'title': '',
    'description': '',
    'price': '',
    'imageUrl': ''
  };

  var _isInit = true;
  var _isLoading = false;

  void _setLoader(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  Future<void> _saveForm() async {
    _setLoader(true);
    _form.currentState.validate();
    _form.currentState.save();
    if (_product.id != null) {
      await Provider.of<Products>(context, listen: false)
          .updateProduct(_product.id, _product);
      _setLoader(false);
      Navigator.of(context).pop();
    } else {
      try {
        await Provider.of<Products>(context, listen: false)
            .addProduct(_product);
      } catch (error) {
        await showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('An error occured'),
                  content: Text(
                      'Sorry something went wrong while attempting to connect to the server!'),
                  actions: [
                    FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('Okay'))
                  ],
                ));
      }
      Navigator.of(context).pop();
      _setLoader(false);
    }
  }

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updatemageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null)
        _product = Provider.of<Products>(context).findById(productId);
      _imageUrlController.text = _product.imageUrl;
      _initValues = {
        'title': _product.title,
        'description': _product.description,
        'price': _product.price.toString(),
        'imageUrl': ''
      };
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  void _updatemageUrl() {
    print(_imageUrlController.text);
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {});
      print(_imageUrlController.text);
    }
  }

  @override
  void dispose() {
    _imageUrlFocusNode.removeListener(_updatemageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Products'),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                _saveForm();
              })
        ],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: EdgeInsets.all(16),
              child: Form(
                  key: _form,
                  child: ListView(children: [
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please provide a value';
                        }
                        return null;
                      },
                      initialValue: _initValues['title'],
                      decoration: InputDecoration(labelText: 'Title'),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_priceFocusNode);
                      },
                      onSaved: (value) {
                        _product = Product(
                            title: value,
                            id: _product.id,
                            isFavourite: _product.isFavourite,
                            description: _product.description,
                            imageUrl: _product.imageUrl,
                            price: _product.price);
                      },
                    ),
                    TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please provide a price';
                          }

                          if (double.tryParse(value) == null) {
                            return 'Please enter a valid number';
                          }

                          if (double.parse(value) <= 0) {
                            return 'Please enter a number greater than 0';
                          }

                          return null;
                        },
                        initialValue: _initValues['price'],
                        decoration: InputDecoration(labelText: 'Price'),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        focusNode: _priceFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_descriptionFocusNode);
                        },
                        onSaved: (value) {
                          _product = Product(
                              title: _product.title,
                              id: _product.id,
                              isFavourite: _product.isFavourite,
                              description: _product.description,
                              imageUrl: _product.imageUrl,
                              price: double.parse(value));
                        }),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a description';
                        }

                        if (value.length < 10) {
                          return 'Should be atleast be 10 characters long';
                        }

                        return null;
                      },
                      initialValue: _initValues['description'],
                      decoration: InputDecoration(labelText: 'Description'),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.multiline,
                      maxLines: 3,
                      onSaved: (value) {
                        _product = Product(
                            title: _product.title,
                            id: _product.id,
                            isFavourite: _product.isFavourite,
                            description: value,
                            imageUrl: _product.imageUrl,
                            price: _product.price);
                      },
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: 100,
                          height: 100,
                          margin: EdgeInsets.only(top: 8, right: 10),
                          decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey)),
                          child: _imageUrlController.text.isEmpty
                              ? Text('Enter Image URL')
                              : FittedBox(
                                  child: Image.network(
                                    _imageUrlController.text,
                                    fit: BoxFit.cover,
                                  ),
                                  fit: BoxFit.cover,
                                ),
                        ),
                        Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter an valide image URL';
                              }

                              return null;
                            },
                            decoration: InputDecoration(labelText: 'Image Url'),
                            keyboardType: TextInputType.url,
                            textInputAction: TextInputAction.done,
                            controller: _imageUrlController,
                            focusNode: _imageUrlFocusNode,
                            onSaved: (value) {
                              _product = Product(
                                  title: _product.title,
                                  id: _product.id,
                                  isFavourite: _product.isFavourite,
                                  description: _product.description,
                                  imageUrl: value,
                                  price: _product.price);
                            },
                            onFieldSubmitted: (value) {
                              _saveForm();
                            },
                          ),
                        )
                      ],
                    )
                  ])),
            ),
    );
  }
}
