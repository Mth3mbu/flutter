import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/products.dart';
import '../widgets/app_drawer.dart';
import '../screens/product_screen.dart';
import '../widgets/user_product_item.dart';

class UserProductsScreen extends StatelessWidget {
  static const routeName = '/user-producs';
  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<Products>(context, listen: false).fetchProductsByUserId();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Proucts'),
        actions: [
          IconButton(
              icon: const Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed(ProductScreen.routeName);
              })
        ],
      ),
      drawer: AppDrawer(),
      body: RefreshIndicator(
        onRefresh: () => _refreshProducts(context),
        child: Padding(
            padding: EdgeInsets.all(8),
            child: FutureBuilder(
                future: Provider.of<Products>(context, listen: false)
                    .fetchProductsByUserId(),
                builder: (ctx, dataSnapShot) {
                  if (dataSnapShot.connectionState == ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    if (dataSnapShot.hasError) {
                      return Center(
                        child: Text(
                            'Ooops! Something went wrong while attemptiong to conncect to the server'),
                      );
                    }
                  }
                  return Consumer<Products>(
                      builder: (ctx, products, _) => products.userItems.length >
                              0
                          ? ListView.builder(
                              itemCount: products.userItems.length,
                              itemBuilder: (ctx, index) => Column(
                                    children: [
                                      UserProductItem(
                                          products.userItems[index].title,
                                          products.userItems[index].imageUrl,
                                          products.userItems[index].id),
                                      Divider()
                                    ],
                                  ))
                          : Center(
                              child: Text('You don\'t have any products'),
                            ));
                })),
      ),
    );
  }
}
