import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/screens/cart_screen.dart';
import '../widgets/products_grid.dart';
import '../widgets/badge.dart';
import '../providers/cart.dart';
import './cart_screen.dart';
import '../widgets/app_drawer.dart';
import '../providers/products.dart';

enum FilterOptions { Favourites, All }

class ProductsOverviwScreen extends StatefulWidget {
  @override
  _ProductsOverviwScreenState createState() => _ProductsOverviwScreenState();
}

class _ProductsOverviwScreenState extends State<ProductsOverviwScreen> {
  bool _showFavouritesOnly = false;
  var _isInit = true;
  var _isLoading = false;

  void toggleFilter(int selectedIndex) {
    setState(() {
      if (selectedIndex == FilterOptions.Favourites.index) {
        _showFavouritesOnly = true;
      } else {
        _showFavouritesOnly = false;
      }
    });
  }

  void _toggleSpinner(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _toggleSpinner(true);
      Provider.of<Products>(context).fetchProducts().then((_) {
        _toggleSpinner(false);
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Shop'),
          actions: [
            PopupMenuButton(
              icon: Icon(Icons.more_vert),
              itemBuilder: (_) => [
                PopupMenuItem(
                  child: Text('Only Favourites'),
                  value: FilterOptions.Favourites.index,
                ),
                PopupMenuItem(
                  child: Text('Show All'),
                  value: FilterOptions.All.index,
                )
              ],
              onSelected: (selectedIndex) {
                toggleFilter(selectedIndex);
              },
            ),
            Consumer<Cart>(
              builder: (ctx, cartObj, ch) =>
                  Badge(child: ch, value: cartObj.itemCount.toString()),
              child: IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () {
                  Navigator.of(context).pushNamed(CartScreen.routeName);
                },
              ),
            )
          ],
        ),
        drawer: AppDrawer(),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : ProductsGrid(_showFavouritesOnly));
  }
}
