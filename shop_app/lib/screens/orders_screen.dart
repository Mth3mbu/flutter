import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/orders.dart';
import '../widgets/order_item.dart';
import '../widgets/app_drawer.dart';

class OrderScreen extends StatelessWidget {
  static const routeName = '/orders';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Your Orders'),
        ),
        drawer: AppDrawer(),
        body: FutureBuilder(
            future:
                Provider.of<Orders>(context, listen: false).fetchAndSetOrders(),
            builder: (ctx, dataSnapShot) {
              if (dataSnapShot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                if (dataSnapShot.hasError) {
                  return Center(
                    child: Text(
                        'Ooops! Something went wrong while attemptiong to conncect to the server'),
                  );
                } else
                  return Consumer<Orders>(
                      builder: (ctx, orders, child) => orders.orders.length > 0
                          ? ListView.builder(
                              itemCount: orders.orders.length,
                              itemBuilder: (ctx, index) =>
                                  OrderItem(orders.orders[index]))
                          : Center(
                              child: Text('No orders found'),
                            ));
              }
            }));
  }
}
