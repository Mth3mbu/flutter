import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import '../providers/cart.dart' as item;
import 'package:http/http.dart' as http;
import 'dart:convert';

class Order {
  final String id;
  final double amount;
  final List<item.CartItem> products;
  final DateTime dateTime;

  Order(
      {@required this.id,
      @required this.amount,
      @required this.products,
      @required this.dateTime});
}

class Orders with ChangeNotifier {
  final String token;
  final String userId;
  var _url = '';
  List<Order> _orders = [];

  Orders(this.token, this._orders, this.userId) {
    _url =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/$userId/orders.json?auth=$token';
  }

  List<Order> get orders {
    return [..._orders];
  }

  List<item.CartItem> extractProducts(order) {
    var products = (order['products'] as List<dynamic>)
        .map((product) => item.CartItem(
            id: product['id'],
            title: product['title'],
            quantity: product['quantity'],
            price: product['price']))
        .toList();

    return products;
  }

  Future<void> fetchAndSetOrders() async {
    final List<Order> loadedOrders = [];
    final response = await http.get(Uri.parse(_url));
    final extractedResponse =
        json.decode(response.body.trim()) as Map<String, dynamic>;
    if (extractedResponse == null) {
      return;
    }

    extractedResponse.forEach((orderId, order) {
      loadedOrders.add(Order(
        id: orderId,
        dateTime: DateTime.parse(order['dateTime']),
        amount: order['amount'],
        products: extractProducts(order),
      ));
    });

    _orders = loadedOrders;
    notifyListeners();
  }

  Future<void> addOrder(List<item.CartItem> cartProducts, double total) async {
    final timeStamp = DateTime.now();
    final response = await http.post(Uri.parse(_url),
        body: json.encode({
          'amount': total,
          'products': cartProducts
              .map((cp) => {
                    'id': cp.id,
                    'title': cp.title,
                    'quantity': cp.quantity,
                    'price': cp.price
                  })
              .toList(),
          'dateTime': timeStamp.toIso8601String()
        }));
    _orders.insert(
        0,
        Order(
            id: json.decode(response.body)['name'],
            amount: total,
            products: cartProducts,
            dateTime: timeStamp));
    notifyListeners();
  }
}
