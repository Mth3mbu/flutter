import 'dart:convert';
import 'package:flutter/material.dart';
import 'product.dart';
import 'package:http/http.dart' as http;
import '../models/http_exception.dart';

class Products with ChangeNotifier {
  List<Product> _items = [];
  List<Product> _userItems = [];
  final String authToken;
  final String userId;

  Products(this.authToken, this._items, this.userId);

  List<Product> get items {
    return [..._items];
  }

  List<Product> get userItems {
    return [..._userItems];
  }

  List<Product> get favourietItems {
    return _items.where((product) => product.isFavourite).toList();
  }

  Product findById(String id) {
    return items.firstWhere((product) => product.id == id);
  }

  Future<void> fetchProductsByUserId() async {
    var baseUrl =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/products.json?auth=$authToken&orderBy="creatorId"&equalTo="$userId"';
    try {
      final response = await http.get(Uri.parse(baseUrl));
      var decodedData = json.decode(response.body) as Map<String, dynamic>;
      if (decodedData == null) {
        return;
      }
      baseUrl =
          'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/userFavourite/$userId.json?auth=$authToken';

      final favoriteResponse = await http.get(Uri.parse(baseUrl));
      final favouriteData =
          json.decode(favoriteResponse.body) as Map<String, dynamic>;

      final List<Product> loadedProducts = [];
      decodedData.forEach((productId, prodData) {
        var productFavStatus =
            favouriteData == null ? null : favouriteData[productId];
        loadedProducts.add(Product(
            id: productId,
            title: prodData['title'],
            description: prodData['description'],
            isFavourite: productFavStatus == null
                ? false
                : productFavStatus['isFavourite'] ?? false,
            imageUrl: prodData['imageUrl'],
            price: prodData['price']));
      });
      _userItems = loadedProducts;

      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  void clearProducts() {
    _items = [];
  }

  Future<void> fetchProducts() async {
    var baseUrl =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.get(Uri.parse(baseUrl));
      var decodedData = json.decode(response.body) as Map<String, dynamic>;
      if (decodedData == null) {
        return;
      }
      baseUrl =
          'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/userFavourite/$userId.json?auth=$authToken';

      final favoriteResponse = await http.get(Uri.parse(baseUrl));
      final favouriteData =
          json.decode(favoriteResponse.body) as Map<String, dynamic>;

      final List<Product> loadedProducts = [];
      decodedData.forEach((productId, prodData) {
        var productFavStatus =
            favouriteData == null ? null : favouriteData[productId];
        loadedProducts.add(Product(
            id: productId,
            title: prodData['title'],
            description: prodData['description'],
            isFavourite: productFavStatus == null
                ? false
                : productFavStatus['isFavourite'] ?? false,
            imageUrl: prodData['imageUrl'],
            price: prodData['price']));
      });
      _items = loadedProducts;
    } catch (error) {
      throw error;
    }
  }

  Future<void> addProduct(Product proudct) async {
    final baseUrl =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/products.json?auth=$authToken';
    try {
      final response = await http.post(Uri.parse(baseUrl),
          body: json.encode({
            'title': proudct.title,
            'description': proudct.description,
            'price': proudct.price,
            'imageUrl': proudct.imageUrl,
            'creatorId': userId
          }));

      final newProdcut = Product(
          id: json.decode(response.body)['name'],
          title: proudct.title,
          description: proudct.description,
          price: proudct.price,
          imageUrl: proudct.imageUrl);

      _userItems.add(newProdcut);

      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateProduct(String productId, Product updatedProduct) async {
    final baseUrl =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/products/${productId}.json?auth=$authToken';
    await http.patch(Uri.parse(baseUrl),
        body: json.encode({
          'title': updatedProduct.title,
          'description': updatedProduct.description,
          'price': updatedProduct.price,
          'imageUrl': updatedProduct.imageUrl,
          'isFavourite': updatedProduct.isFavourite
        }));
    final productIndex = _userItems.indexWhere((prod) => prod.id == productId);
    if (productIndex >= 0) {
      _userItems[productIndex] = updatedProduct;
      notifyListeners();
    }
  }

  void deleteProduct(String productId) {
    final baseUrl =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/products/${productId}.json?auth=$authToken';
    final existingProductIndex =
        _userItems.indexWhere((product) => product.id == productId);
    var existingProduct = _userItems[existingProductIndex];
    _userItems.removeAt(existingProductIndex);
    http.delete(Uri.parse(baseUrl)).then((response) {
      if (response.statusCode >= 400) {
        _userItems.insert(existingProductIndex, existingProduct);
        throw HttpException('Could not delete product');
      }
      existingProduct = null;
      notifyListeners();
    }).catchError((error) {
      _userItems.insert(existingProductIndex, existingProduct);
      notifyListeners();
    });
    _items.removeWhere((product) => product.id == productId);
  }
}
