import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavourite;

  Product(
      {@required this.id,
      @required this.title,
      @required this.description,
      @required this.price,
      @required this.imageUrl,
      this.isFavourite = false});

  void _setFavValue(bool oldStatus) {
    isFavourite = oldStatus;
  }

  Future<void> toggleFavouriteStatus(String token, String userId) async {
    final url =
        'https://flutter-shop-app-57d39-default-rtdb.firebaseio.com/userFavourite/$userId/${id}.json?auth=$token';
    final oldStatus = isFavourite;
    isFavourite = !isFavourite;
    try {
      notifyListeners();
      final response = await http.put(Uri.parse(url),
          body: json.encode({'isFavourite': isFavourite}));

      if (response.statusCode >= 400) {
        _setFavValue(oldStatus);
      }
    } catch (error) {
      _setFavValue(oldStatus);
    }
  }
}
